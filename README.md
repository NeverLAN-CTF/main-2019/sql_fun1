# SQL Fun 1

#### Description
REPORT: 'My Customer forgot his `Password`. His `Fname` is Jimmy. Can you get his password for me? It should be in the `users` table'

#### Flag
flag{SQL_F0R_Th3_W1n}

#### Hints
It's not an injection challenge, just a regular sql terminal

#### Solution
Query: SELECT * FROM users WHERE Fname="Jimmy"
