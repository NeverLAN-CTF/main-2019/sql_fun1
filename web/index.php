<?php

$servername = "127.0.0.1";
$username = "sql_fun1";
$password = "<password_web>"; // This is set by Docker when building the container
$database = "sql_fun1";

$conn = mysqli_connect($servername, $username, $password, $database);

    if(!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

if(array_key_exists("Query",$_POST)){
    $query= $_POST["Query"];
	if(preg_match('/.*WHERE.*/i', $query)){
        if($data = $conn->query($query)){
			echo('<table border="1"><thead><tr>');
			$res = $data->fetch_fields();
			foreach($res as $val){
				echo('<th>');
				echo($val->name);
				echo('</th>');
			}
			echo('</tr></thead></tbody>');
		    while($dataArray = $data->fetch_assoc()){
				echo('<tr>');
	            foreach($dataArray as $key => $value){
	                echo('<td>'.$value.'</td>');
	            }
				echo('</tr>');
			}
			echo('</tbody></table>');
        }else{
            echo('Your query could not be submitted.');
            echo('<br/>Error: '.mysqli_errno($conn));
        }
	}else{
        echo('Not quite. Your query must have a WHERE clause.');
    }
}

?>

<!DOCTYPE html>
<html>
    <head><title>SQL fun 1</title></head>
    <body>
        <p id="p">REPORT: 'My Customer forgot his `Password`. His `Fname` is Jimmy. Can you get his password for me? It should be in the `users` table'</p>
        <form method="POST" action="?">
            <textarea name="Query"></textarea>
            <input type="submit" id="submitButton"/>
        </form>
    </body>
</html>
