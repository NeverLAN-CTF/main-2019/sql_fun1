# start mysql
#/usr/bin/supervisord -c /etc/supervisord.conf
#/usr/bin/supervisorctl -c /etc/supervisord.conf start mariadb
echo "*** STARTING DB ***"
/usr/bin/mysqld_safe &
sleep 3

# import database
echo "*** IMPORTING DB ***"
mysql < /tmp/db.sql

echo "*** KILLING DB **"
kill $!
#pkill -f /usr/bin/mysqld_safe
# kill daemon version of supervisord
#/usr/bin/supervisorctl -c /etc/supervisord.conf stop mariadb
#pkill -f supervisord  # kill it

# remove passwords from temp directory
#rm /tmp/pass_root /tmp/pass_web /tmp/db.sql

# finally run the supervisord 
echo "*** STARTING supervisord ***"
/usr/bin/supervisord -n -c /etc/supervisord.conf
